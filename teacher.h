#pragma once
#include<iostream>
#include<vector>
using namespace std;
#include"orderFile.h"
#include"identity.h"

class teacher:public Identity{
	public:
		teacher();
		teacher(int empId,string name,string pwd);
		virtual void openMenu();
		void validOrder();
		void showAllOrder();
		int m_EmpId;
};
