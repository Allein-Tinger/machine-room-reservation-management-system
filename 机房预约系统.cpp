#include<iostream>
#include<fstream>
#include<cstring>
#include<string>
#include"identity.h"
#include"globalFile.h"
#include"student.h"
#include"teacher.h"
#include"manager.h"
using namespace std;

void managerMenu(Identity * &Manager){
	while(true){
		Manager->openMenu();
		manager *man =(manager *)Manager;
		int select=0;
		cin>>select;
		if(select==1){
			cout<<"添加账号"<<endl;
			man->addPerson(); 
		}
		else if(select==2){
			cout<<"查看账号"<<endl; 
			man->showPerson();
		}
		else if(select==3){
			cout<<"查看机房"<<endl;
			man->showComputer(); 
		}
		else if(select==4){
			cout<<"清空预约"<<endl;
			man->cleanFile(); 
		}
		else{
			delete Manager;
			cout<<"注销成功"<<endl;
			system("pause");
			system("cls");
			return; 
		}
	}
}

void studentMenu(Identity * &Student){
	while(true){
		Student->openMenu();
		student *stu =(student *)Student;
		int select=0;
		cin>>select;
		if(select==1){
			cout<<"申请预约"<<endl;
			stu->applyOrder(); 
		}
		else if(select==2){
			cout<<"查看我的预约"<<endl; 
			stu->showMyOrder();
		}
		else if(select==3){
			cout<<"查看所有预约"<<endl;
			stu->showAllOrder(); 
		}
		else if(select==4){
			cout<<"取消预约"<<endl;
			stu->cancelOrder(); 
		}
		else{
			delete Student;
			cout<<"注销成功"<<endl;
			system("pause");
			system("cls");
			return; 
		}
	}
}

void teacherMenu(Identity * &Teacher)
{
	while (true)
	{
		//调用子菜单的界面
		Teacher->openMenu();

		teacher *tea = (teacher*)Teacher;

		int select = 0;
		cin >> select;

		if (select == 1)  //查看所有预约
		{
			tea->showAllOrder();
		}
		else if (select == 2)  //审核预约
		{
			tea->validOrder();
		}
		else  //注销登录
		{
			delete Teacher;
			cout << "注销成功！" << endl;
			system("pause");
			system("cls");
			return;
		}
	}
}

void LoginIn(string fileName,int type) {
	char s[20];
	strcpy(s,fileName.c_str());
	Identity * Person = NULL;
	ifstream ifs;
	ifs.open(s,ios::in);
	if(!ifs.is_open()) {
		cout<<"文件不存在"<<endl;
		ifs.close();
		return;
	}
	int id=0;
	string name;
	string pwd;
	if(type==1) {
		cout<<"请输入你的学号: "<<endl;
		cin>>id;
	} else if(type==2) {
		cout<<"请输入你的职工号: "<<endl;
		cin>>id;
	}
	cout<<"请输入用户名: "<<endl;
	cin>>name;
	cout<<"请输入密码: "<<endl;
	cin>>pwd;
	if(type==1) {
		int fId;
		string fname;
		string fpwd;
		while(ifs>>fId&&ifs>>fname&&ifs>>fpwd){
			if(fId==id&&fname==name&&fpwd==pwd){
				cout<<"学生登录验证成功!"<<endl;
				system("pause");
				system("cls");
				Person = new student(id,name,pwd);
				studentMenu(Person);
				return; 
			}
		}
	} 
	else if(type==2) {
			int fId;
		string fname;
		string fpwd;
		while(ifs>>fId&&ifs>>fname&&ifs>>fpwd){
			if(fId==id&&fname==name&&fpwd==pwd){
				cout<<"老师登录验证成功!"<<endl;
				system("pause");
				system("cls");
				Person = new teacher(id,name,pwd);
				teacherMenu(Person);
				return; 
			}
		}

	} 
	else if(type==3) {
		string fname;
		string fpwd;
		while(ifs>>fname&&ifs>>fpwd){
			if(fname==name&&fpwd==pwd){
				cout<<"管理员登录验证成功!"<<endl;
				system("pause");
				system("cls");
				Person = new manager(name,pwd);
				managerMenu(Person);
				return; 
			}
		}
	}
	cout<<"验证登录失败"<<endl;
	system("pause");
	system("cls");
	return;
}

int main() {
	int select=0;
	while(true) {
		cout << "=====================  欢迎来到机房预约系统  ====================" << endl;
		cout << endl << "请选择您的身份" << endl;
		cout << "\t\t -------------------------------\n";
		cout << "\t\t|                               |\n";
		cout << "\t\t|          1.学生代表           |\n";
		cout << "\t\t|                               |\n";
		cout << "\t\t|          2.老    师           |\n";
		cout << "\t\t|                               |\n";
		cout << "\t\t|          3.管 理 员           |\n";
		cout << "\t\t|                               |\n";
		cout << "\t\t|          0.退    出           |\n";
		cout << "\t\t|                               |\n";
		cout << "\t\t -------------------------------\n";
		cout << "输入您的选择：";
		cin>>select;
		switch(select) {
			case 1:
				LoginIn(STUDENT_FILE,1);
				break;
			case 2:
				LoginIn(TEACHER_FILE,2);
				break;
			case 3:
				LoginIn(ADMIN_FILE,3);
				break;
			case 0:
				cout<<"欢迎下次使用!"<<endl;
				system("pause");
				return 0;
				break;
			default:
				cout<<"输入有误请重新选择!"<<endl;
				system("pause");
				system("cls");
				break;
		}
	}

	system("pause");
	return 0;
}
