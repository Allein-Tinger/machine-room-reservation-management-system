#pragma once
#include<iostream>
#include<string>
#include<fstream> 
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;
#include"identity.h"
#include"student.h"
#include"teacher.h"
#include"computerRoom.h"

class manager:public Identity{
	public:
		manager();
		manager(string name,string pwd);
		virtual void openMenu();
		void addPerson();
		void showPerson();
		void showComputer();
		void cleanFile();
		void initVector();
		bool checkRepeat(int id,int type);
		vector<student>vStu;
		vector<teacher>vTea;
		vector<ComputerRoom>vCom;
};
