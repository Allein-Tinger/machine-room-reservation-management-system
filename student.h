#pragma once
#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
#include<fstream>
#include"computerRoom.h"
#include"globalFile.h"
using namespace std;
#include"identity.h"
#include"orderFile.h"

class student:public Identity{
	public:
		student();
		student(int id,string name,string pwd);
		virtual void openMenu();
		void applyOrder();
		void showMyOrder();
		void showAllOrder();
		void cancelOrder();
		int m_Id;
		vector<ComputerRoom>vCom;
};
